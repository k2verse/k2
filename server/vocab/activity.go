package vocab

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
	"strings"
)

type ActivityType string

var (
	errNoObject            = errors.New("object not provided")
	errNoActivityType      = errors.New("activity type not provided")
	errInvalidActivityType = errors.New("invalid activity type provided")
	errNoContext           = errors.New("context not provided")
	errInvalidContext      = errors.New("invalid context provided")
)

const (
	ActivityTypeUnknown ActivityType = "Unknown"
	ActivityTypeFollow               = "Follow"
	ActivityTypeAccept               = "Accept"
)

func toActivityType(activityType string) ActivityType {
	t := strings.ToLower(activityType)
	switch t {
	case "follow":
		return ActivityTypeFollow
	case "accept":
		return ActivityTypeAccept
	default:
		return ActivityTypeUnknown
	}
}

type Activity struct {
	Object `mapstructure:",squash"`
	Actor  *Actor  `json:"actor"`
	Obj    *Object `json:"object" mapstructure:"object"`
}

func NewAcceptActivity(acceptWhat ActivityType) *Activity {
	obj := Object{
		Context: []Context{CtxActivityStreams},
		Type:    ActivityTypeAccept,
		ID:      "",
	}
	return &Activity{
		Object: obj,
		Obj: &Object{
			Type: ObjectType(acceptWhat),
		},
	}
}

func ToActivity(data []byte) (*Activity, error) {
	var jsonMap map[string]interface{}
	if err := json.Unmarshal(data, &jsonMap); err != nil {
		return nil, err
	}

	var activity Activity

	t, ok := jsonMap["type"].(string)
	if !ok {
		return nil, errNoActivityType
	}
	at := toActivityType(t)
	if at == ActivityTypeUnknown {
		return nil, errInvalidActivityType
	}
	activity.Type = toObjectType(at)

	ctx, ok := jsonMap["@context"].(string)
	if !ok {
		return nil, errNoContext
	}
	c := ToContext(ctx)
	if c == CtxUnknown {
		return nil, errInvalidContext
	}
	activity.Context = []Context{c}

	summary, ok := jsonMap["summary"].(string)
	if ok {
		activity.Summary = summary
	}
	actor, err := extractActor(jsonMap)
	if err != nil {
		return nil, fmt.Errorf("failed to extract actor: %w", err)
	}
	activity.Actor = actor

	obj, err := extractObject(jsonMap)
	if err != nil {
		return nil, fmt.Errorf("failed to extract object: %w", err)
	}
	activity.Obj = obj
	return &activity, nil
}

func extractActor(m map[string]interface{}) (*Actor, error) {
	var actor *Actor
	if a, ok := m["actor"]; ok {
		switch v := a.(type) {
		case string:
			actor = &Actor{
				Object: Object{
					ID: v,
				},
			}
		case map[string]interface{}:
			if err := mapstructure.Decode(a, &actor); err != nil {
				return nil, fmt.Errorf("failed to decode map to struct: %w", err)
			}
		default:
			logrus.Debugf("some other type: %v", v)
			return nil, errors.New("invalid actor type")
		}
	}
	if actor == nil {
		return nil, errNoObject
	}
	return actor, nil
}

func extractObject(m map[string]interface{}) (*Object, error) {
	var object *Object
	if o, ok := m["object"]; ok {
		switch v := o.(type) {
		case string:
			object = &Object{
				ID: v,
			}
		case map[string]interface{}:
			if err := mapstructure.Decode(o, &object); err != nil {
				return nil, fmt.Errorf("failed to decode map to struct: %w", err)
			}
		default:
			logrus.Debugf("some other type: %v", v)
			return nil, errors.New("invalid object type")
		}
	}
	if object == nil {
		return nil, errNoObject
	}
	return object, nil
}
