package vocab

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/url"
	"path"
)

type category int

var ErrBadDomain = errors.New("invalid domain")

const (
	categoryInternal category = iota
	categoryFollower
)

const (
	EndpointFollowers = "followers.json"
	EndpointFollowing = "following.json"
	EndpointLiked     = "liked.json"
	EndpointInbox     = "inbox.json"
	EndpointOutbox    = "outbox.json"
)

type PublicKey struct {
	ID           string `bson:"-" json:"id"`
	Owner        string `bson:"-" json:"owner"`
	PublicKeyPEM []byte `bson:"-" json:"publicKeyPem"`
}

func NewPublicKey(ownerID string, pem []byte) *PublicKey {
	return &PublicKey{
		ID:           fmt.Sprintf("%s#main-key", ownerID),
		Owner:        ownerID,
		PublicKeyPEM: pem,
	}
}

type Actor struct {
	Object            `mapstructure:",squash"`
	PreferredUsername string     `bson:"preferredUsername,omitempty" json:"preferredUsername,omitempty"`
	Name              string     `bson:"name,omitempty" json:"name,omitempty"`
	url               *url.URL   `bson:"-" json:"-"`
	PublicKey         *PublicKey `bson:"-" json:"publicKey,omitempty"`
	Icon              string     `bson:"icon" json:"icon,omitempty"`
	category          category
}

func NewActorPersonWithID(id string) *Actor {
	return &Actor{
		Object: Object{ID: id},
	}
}

func NewActor(kind ObjectType, domain string, username string) (*Actor, error) {
	u, err := url.Parse(domain)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", err.Error(), ErrBadDomain)
	}
	if u.Scheme == "" {
		return nil, fmt.Errorf("provided scheme was empty: %w", ErrBadDomain)
	}
	u.Path = path.Join(u.Path, username)

	id := u.String()

	obj := Object{
		Context: []Context{CtxActivityStreams},
		Type:    kind,
		ID:      id,
	}
	return &Actor{
		Object:            obj,
		PreferredUsername: username,
		url:               u,
		category:          categoryInternal,
	}, nil
}

func (a *Actor) getURL() url.URL {
	if a.url != nil {
		return *a.url
	}
	u, err := url.Parse(a.ID)
	if err != nil {
		logrus.Fatal("failed to parse user ID as URL: %w", err)
	}
	a.url = u
	return *u
}
func (a *Actor) Following() string {
	url := a.getURL()
	url.Path = path.Join(url.Path, EndpointFollowing)
	return url.String()
}

func (a *Actor) Followers() string {
	url := a.getURL()
	url.Path = path.Join(url.Path, EndpointFollowers)
	return url.String()
}

func (a *Actor) Liked() string {
	url := a.getURL()
	url.Path = path.Join(url.Path, EndpointLiked)
	return url.String()
}

func (a *Actor) Inbox() string {
	url := a.getURL()
	url.Path = path.Join(url.Path, EndpointInbox)
	return url.String()
}

func (a *Actor) Outbox() string {
	url := a.getURL()
	url.Path = path.Join(url.Path, EndpointOutbox)
	return url.String()
}

func (a *Actor) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Actor
		Following string `json:"following"`
		Followers string `json:"followers"`
		Liked     string `json:"liked"`
		Inbox     string `json:"inbox"`
		Outbox    string `json:"outbox"`
	}{
		Actor:     *a,
		Following: a.Following(),
		Followers: a.Followers(),
		Liked:     a.Liked(),
		Inbox:     a.Inbox(),
		Outbox:    a.Outbox(),
	})
}

/*
func (a *Actor) UnmarshalJSON(bytes []byte) error {
	var name string
	err := json.Unmarshal(bytes, &name)
	if err != nil {
		return err
	}

	return nil
}
*/