package vocab

import "strings"

type Context string

const (
	CtxUnknown         Context = "unknown"
	CtxActivityStreams         = "https://www.w3.org/ns/activitystreams"
)

func ToContext(s string) Context {
	switch strings.ToLower(s) {
	case "https://www.w3.org/ns/activitystreams":
		return CtxActivityStreams
	default:
		return CtxUnknown
	}
}
