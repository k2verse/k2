package vocab

import "encoding/json"

const (
	typeCollection = "Collection"
)
const (
	summaryFollowers = "Followers of this actor"
)

type Follower struct {
	ID string `json:"id"`
}

type Collection struct {
	Context Context `json:"@context,omitempty"`
	Summary string  `json:"summary,omitempty"`
	Type    string  `json:"type,omitempty"`
}

type FollowersCollection struct {
	Collection
	Items []Follower `json:"items"`
}

func NewFollowersCollection() *FollowersCollection {
	return &FollowersCollection{
		Collection: Collection{
			Context: CtxActivityStreams,
			Summary: summaryFollowers,
			Type:    typeCollection,
		},
	}
}

func (f *FollowersCollection) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		FollowersCollection
		TotalItems int `json:"totalItems"`
	}{
		FollowersCollection: *f,
		TotalItems:          len(f.Items),
	})
}
