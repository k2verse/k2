package vocab

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestToActivity(t *testing.T) {
	t.Parallel()
	testsCases := []struct {
		name         string
		inputJSON    string
		wantErr      error
		wantActivity *Activity
	}{
		{
			name: "Follow activity with full objects",
			inputJSON: `{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "Sally followed John",
			  "type": "Follow",
			  "actor": {
				"id": "actor-id"
			  },
			  "object": {
				"id": "object-id"
			  }
			}`,
			wantActivity: &Activity{
				Object: Object{
					Context: []Context{CtxActivityStreams},
					Type:    ActivityTypeFollow,
					Summary: "Sally followed John",
				},
				Actor: &Actor{
					Object: Object{
						ID: "actor-id",
					},
				},
				Obj: &Object{
					ID: "object-id",
				},
			},
		},
		{
			name: "Follow activity with just ID for actor",
			inputJSON: `{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "Sally followed John",
			  "type": "Follow",
			  "actor": "actor-id",
			  "object": {
				"id": "object-id"
			  }
			}`,
			wantActivity: &Activity{
				Object: Object{
					Context: []Context{CtxActivityStreams},
					Type:    ActivityTypeFollow,
					Summary: "Sally followed John",
				},
				Actor: &Actor{
					Object: Object{
						ID: "actor-id",
					},
				},
				Obj: &Object{
					ID: "object-id",
				},
			},
		},
		{
			name: "Follow activity with just ID for actor and object",
			inputJSON: `{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "Sally followed John",
			  "type": "Follow",
			  "actor": "actor-id",
			  "object": "object-id"
			}`,
			wantActivity: &Activity{
				Object: Object{
					Context: []Context{CtxActivityStreams},
					Type:    ActivityTypeFollow,
					Summary: "Sally followed John",
				},
				Actor: &Actor{
					Object: Object{
						ID: "actor-id",
					},
				},
				Obj: &Object{
					ID: "object-id",
				},
			},
		},
		{
			name: "Follow activity without context",
			inputJSON: `{
			  "summary": "Sally followed John",
			  "type": "Follow",
			  "actor": "actor-id",
			  "object": "object-id"
			}`,
			wantErr: errNoContext,
		},
		{
			name: "Follow activity with invalid context",
			inputJSON: `{
			  "@context": "some-invalid-context",
			  "summary": "Sally followed John",
			  "type": "Follow",
			  "actor": "actor-id",
			  "object": "object-id"
			}`,
			wantErr: errInvalidContext,
		},
		{
			name: "Follow activity without type",
			inputJSON: `{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "Sally followed John",
			  "actor": "actor-id",
			  "object": "object-id"
			}`,
			wantErr: errNoActivityType,
		},
		{
			name: "Follow activity with invalid type",
			inputJSON: `{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "Sally followed John",
			  "type": "Followed",
			  "actor": "actor-id",
			  "object": "object-id"
			}`,
			wantErr: errInvalidActivityType,
		},
	}

	for _, tc := range testsCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			gotActivity, gotErr := ToActivity([]byte(tc.inputJSON))
			require.ErrorIs(t, gotErr, tc.wantErr)
			if gotErr != nil {
				return
			}
			require.EqualValues(t, tc.wantActivity, gotActivity)
		})
	}

}
