package vocab

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestFollowersMarshal(t *testing.T) {
	testCases := []struct {
		name          string
		targetActorID string
		followerIDs   []string
		wantJSON      string
	}{
		{
			name:          "success",
			targetActorID: "alyssa",
			followerIDs:   []string{"domain.com/sally", "otherdomain.com/u/sarah"},
			wantJSON: fmt.Sprintf(`{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "%s",
			  "type": "%s",
			  "totalItems": 2,
			  "items": [
				{
					"id": "domain.com/sally"
				},
				{
					"id": "otherdomain.com/u/sarah"
				}
			]}`, summaryFollowers, typeCollection),
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			fc := NewFollowersCollection()
			for _, f := range tc.followerIDs {
				follower := Follower{ID: f}
				fc.Items = append(fc.Items, follower)
			}
			gotJSON, err := json.Marshal(fc)
			require.NoError(t, err)
			require.JSONEq(t, tc.wantJSON, string(gotJSON))
		})
	}
}
