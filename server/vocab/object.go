package vocab

type ObjectType string

const (
	TypeUnknown     ObjectType = "Unknown"
	TypePerson                 = "Person"
	TypeApplication            = "Application"
	TypeService                = "Service"
	TypeFollow                 = "Follow"
	TypeAccept                 = "Accept"
)

type activityTypeOrStr interface {
	ActivityType | string
}

func toObjectType[V activityTypeOrStr](aot V) ObjectType {
	switch v := any(aot).(type) {
	case string:
		return ObjectType(v)
	case ActivityType:
		switch v {
		case ActivityTypeFollow:
			return TypeFollow
		case ActivityTypeAccept:
			return TypeAccept
		default:
			return TypeUnknown
		}
	default:
		return TypeUnknown
	}
}

type Object struct {
	Context []Context  `bson:"@context,omitempty" json:"@context,omitempty"`
	Type    ObjectType `bson:"type,omitempty" json:"type,omitempty"`
	ID      string     `bson:"id,omitempty" json:"id,omitempty" mapstructure:"id"`
	Summary string     `bson:"summary,omitempty" json:"summary,omitempty"`
}
