package vocab

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestActorMarshal(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name              string
		preferredUsername string
		wantJSON          string
	}{
		{
			name:              "success",
			preferredUsername: "alyssa",
			wantJSON: fmt.Sprintf(`{"@context": ["https://www.w3.org/ns/activitystreams"],
					 "type": "%s",
					 "id": "https://social.example/alyssa",
					 "preferredUsername": "alyssa",
					 "inbox": "https://social.example/alyssa/inbox.json",
					 "outbox": "https://social.example/alyssa/outbox.json",
					 "followers": "https://social.example/alyssa/followers.json",
					 "following": "https://social.example/alyssa/following.json",
					 "liked": "https://social.example/alyssa/liked.json"}`, TypePerson),
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			actor, err := NewActor(TypePerson, "https://social.example/", "alyssa")
			require.NoError(t, err)

			gotJSON, err := json.Marshal(actor)
			require.NoError(t, err)
			require.JSONEq(t, tc.wantJSON, string(gotJSON))
		})
	}
}

func TestEndpoints(t *testing.T) {
	testCases := []struct {
		name          string
		userName      string
		domain        string
		wantID        string
		wantFollowing string
		wantFollowers string
		wantLiked     string
		wantInbox     string
		wantOutbox    string
		wantErr       error
	}{
		{
			name:          "domain-slash",
			userName:      "lazer",
			domain:        "https://mydomain.io/",
			wantID:        "https://mydomain.io/lazer",
			wantFollowing: "https://mydomain.io/lazer/following.json",
			wantFollowers: "https://mydomain.io/lazer/followers.json",
			wantLiked:     "https://mydomain.io/lazer/liked.json",
			wantInbox:     "https://mydomain.io/lazer/inbox.json",
			wantOutbox:    "https://mydomain.io/lazer/outbox.json",
		},
		{
			name:          "domain-no-slash",
			userName:      "lazer",
			domain:        "https://mydomain.io",
			wantID:        "https://mydomain.io/lazer",
			wantFollowing: "https://mydomain.io/lazer/following.json",
			wantFollowers: "https://mydomain.io/lazer/followers.json",
			wantLiked:     "https://mydomain.io/lazer/liked.json",
			wantInbox:     "https://mydomain.io/lazer/inbox.json",
			wantOutbox:    "https://mydomain.io/lazer/outbox.json",
		},
		{
			name:     "domain-no-scheme",
			userName: "lazer",
			domain:   "mydomain.io",
			wantErr:  ErrBadDomain,
		},
		{
			name:     "domain-invalid",
			userName: "lazer",
			domain:   "mydomain/.io",
			wantErr:  ErrBadDomain,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			actor, gotErr := NewActor(TypePerson, tc.domain, tc.userName)
			require.ErrorIs(t, gotErr, tc.wantErr)
			if gotErr != nil {
				return
			}
			require.Equal(t, tc.wantID, actor.ID)
			require.Equal(t, tc.wantFollowers, actor.Followers())
			require.Equal(t, tc.wantFollowing, actor.Following())
			require.Equal(t, tc.wantLiked, actor.Liked())
			require.Equal(t, tc.wantInbox, actor.Inbox())
			require.Equal(t, tc.wantOutbox, actor.Outbox())
		})
	}
}
