package server

import (
	"github.com/sirupsen/logrus"
	"k2/server/keyring"
	"k2/server/service"
	"k2/server/storage/mongo"
)

type Server struct {
	router *service.Service
}

func NewServer(keyRing keyring.KeyRing, domain string, port int, uri, dbName string) *Server {
	mongo := mongo.NewMongo(uri, dbName, keyRing)
	s := Server{
		router: service.New(port, mongo, domain),
	}
	return &s
}

func (s *Server) Start() {
	logrus.Info("starting server")
	s.router.Serve()
}
