package mongo

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	actor2 "k2/server/vocab"
)

const actorCollection = "actors"

func (m *Mongo) StoreActor(ctx context.Context, actor *actor2.Actor) error {
	// Generate keypair
	if m.keyRing != nil {
		pubKey, err := m.keyRing.GenerateKeyPair(actor.PreferredUsername)
		if err != nil {
			return fmt.Errorf("failed to generate key pair: %w", err)
		}
		actor.PublicKey = actor2.NewPublicKey(actor.ID, pubKey)
	} else {
		logrus.Warn("No keyring specified. Ensure this is not running in production.")
	}

	collection := m.db.Collection(actorCollection)
	res, err := collection.InsertOne(ctx, actor)
	if err != nil {
		return fmt.Errorf("%s: %w", err.Error(), ErrActorInsertFailed)
	}
	logrus.Infof("inserted actor: %v", res)
	return nil
}

func (m *Mongo) GetActor(ctx context.Context, id string) (*actor2.Actor, error) {
	collection := m.db.Collection(actorCollection)
	var actor actor2.Actor
	if err := collection.FindOne(ctx, bson.D{{"id", id}}).Decode(&actor); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, ErrActorNotFound
		}
		return nil, fmt.Errorf("failed to get vocab: %w", err)
	}
	// Get public key
	if m.keyRing != nil {
		publicKey, err := m.keyRing.GetPublicKey(actor.PreferredUsername)
		if err != nil {
			return nil, fmt.Errorf("failed to get public key: %w", err)
		}
		actor.PublicKey = actor2.NewPublicKey(id, publicKey)
	}
	return &actor, nil
}

func (m *Mongo) FindActorByPreferredName(ctx context.Context, preferredName string) (*actor2.Actor, error) {
	collection := m.db.Collection(actorCollection)
	var actor actor2.Actor
	if err := collection.FindOne(ctx, bson.D{{"preferredName", preferredName}}).Decode(&actor); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, ErrActorNotFound
		}
		return nil, fmt.Errorf("failed to get vocab: %w", err)
	}
	// Get public key
	publicKey, err := m.keyRing.GetPublicKey(actor.PreferredUsername)
	if err != nil {
		return nil, fmt.Errorf("failed to get public key: %w", err)
	}
	actor.PublicKey = actor2.NewPublicKey(actor.ID, publicKey)
	return &actor, nil
}
