package mongo

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"k2/server/keyring"
)

type Mongo struct {
	uri     string
	dbName  string
	client  *mongo.Client
	db      *mongo.Database
	keyRing keyring.KeyRing
}

func NewMongo(uri string, dbName string, keyRing keyring.KeyRing) *Mongo {
	return &Mongo{
		uri:     uri,
		dbName:  dbName,
		keyRing: keyRing,
	}
}

func (m *Mongo) Connect(ctx context.Context) error {
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(m.uri))
	if err != nil {
		return fmt.Errorf("failed to connect to Mongo: %w", err)
	}
	m.client = client
	m.db = client.Database(m.dbName)
	return nil
}
