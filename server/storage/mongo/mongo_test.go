package mongo

import (
	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
	"io/ioutil"
	"k2/server/keyring"
)

func getTestMongo(mt *mtest.T) *Mongo {
	ring := getTestKeyring(mt)
	mongo := NewMongo(testMongoURI, testDbName, ring)
	mongo.db = mt.DB
	return mongo
}

func getTestKeyring(mt *mtest.T) keyring.KeyRing {
	// Create local keyring
	keyLoc, err := ioutil.TempDir("", "k2test")
	require.NoError(mt, err)
	ring := keyring.NewLocalRing(keyLoc)
	return ring
}
