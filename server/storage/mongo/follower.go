package mongo

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"k2/server/vocab"
)

const collectionFollowers = "followers"

type Follower struct {
	FollowerID    string
	TargetActorID string
}

// StoreFollower stores a follower in MongoDB
func (m *Mongo) StoreFollower(ctx context.Context, followerID string, targetActorID string) error {
	// Confirm that target ID is a valid actor
	_, err := m.GetActor(ctx, targetActorID)
	if err != nil {
		return fmt.Errorf("target is not a valid actor: %w", ErrActorNotFound)
	}

	collection := m.db.Collection(collectionFollowers)
	f := Follower{
		FollowerID:    followerID,
		TargetActorID: targetActorID,
	}
	res, err := collection.InsertOne(ctx, f)
	if err != nil {
		return fmt.Errorf("%s: %w", err.Error(), ErrFollowerInsertFailed)
	}
	logrus.Infof("inserted Follower: %v", res)
	return nil
}

func (m *Mongo) GetFollowers(ctx context.Context, targetActorID string) (*vocab.FollowersCollection, error) {
	collection := m.db.Collection(collectionFollowers)
	fc := vocab.NewFollowersCollection()

	var followers []Follower
	cur, err := collection.Find(ctx, bson.D{{"TargetActorID", targetActorID}})
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return fc, nil
		}
		return nil, fmt.Errorf("failed to get followers: %w", err)
	}

	if err := cur.All(ctx, &followers); err != nil {
		return nil, fmt.Errorf("failed to decode followers: %w", err)
	}

	for _, f := range followers {
		follower := vocab.Follower{ID: f.FollowerID}
		fc.Items = append(fc.Items, follower)
	}

	return fc, nil
}
