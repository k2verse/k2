package mongo

import "errors"

var (
	ErrActorInsertFailed    = errors.New("failed to insert actor")
	ErrFollowerInsertFailed = errors.New("failed to insert Follower")
	ErrActorNotFound        = errors.New("actor not found")
)
