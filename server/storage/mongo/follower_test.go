package mongo

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
	"k2/server/keyring"
	"k2/server/vocab"
	"testing"
)

const (
	testClnFollowersName = "followers"
)

// TestStoreActor tests storing an Actor instance in MongoDB
func TestStoreFollower(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		// Test case name
		name string
		// Get vocab instance to store
		//	getFollowersCollection func(mt *mtest.T) *vocab.FollowersCollection
		followerID      string
		actorID         string
		setupMongoMocks func(mt *mtest.T, ring keyring.KeyRing)
		// The response we expect Mongo to return
		mongoRes bson.D
		// Desired error
		wantErr error
	}{
		{
			name: "success",
			setupMongoMocks: func(mt *mtest.T, ring keyring.KeyRing) {
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, "alyssa")
				require.NoError(mt, err)

				bsonData, err := bson.Marshal(a)
				require.NoError(mt, err)
				_, err = ring.GenerateKeyPair(a.PreferredUsername)
				require.NoError(mt, err)

				var bsonD bson.D
				err = bson.Unmarshal(bsonData, &bsonD)
				require.NoError(mt, err)
				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.FirstBatch,
					bsonD)
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.NextBatch)
				mt.AddMockResponses(res, end)
			},
			followerID: "https://some-domain.io/u/sara",
			actorID:    "https://domain.io/alyssa",
			// Empty success response
			mongoRes: mtest.CreateSuccessResponse(),
		},
		{
			name: "insert-failure",
			setupMongoMocks: func(mt *mtest.T, ring keyring.KeyRing) {
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, "alyssa")
				require.NoError(mt, err)

				bsonData, err := bson.Marshal(a)
				require.NoError(mt, err)
				_, err = ring.GenerateKeyPair(a.PreferredUsername)
				require.NoError(mt, err)

				var bsonD bson.D
				err = bson.Unmarshal(bsonData, &bsonD)
				require.NoError(mt, err)
				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.FirstBatch,
					bsonD)
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.NextBatch)
				mt.AddMockResponses(res, end)
			},
			// Have Mongo return a write error
			mongoRes: mtest.CreateWriteErrorsResponse(mtest.WriteError{
				Index:   1,
				Code:    123,
				Message: "some error",
			}),
			wantErr: ErrFollowerInsertFailed,
		},
		{
			name:    "nonexistent-actor",
			wantErr: ErrActorNotFound,
		},
	}

	opts := mtest.NewOptions().DatabaseName(testDbName).ClientType(mtest.Mock)

	// Create instance of `mtest.T`, which will contain a
	// test database.
	mt := mtest.New(t, opts)
	defer mt.Close()

	for _, tc := range testCases {
		// Shadow `tc` var to keep relevant test case in scope,
		// since these tests run in parallel
		tc := tc

		// mt.Run() creates a new T and a new collection for
		// the test case
		mt.Run(tc.name, func(mt *mtest.T) {
			mt.Parallel()
			// Create instance of our own Mongo struct, override the
			// database with the test one we created via `mtest.New()`
			mongo := getTestMongo(mt)
			mongo.db = mt.DB

			if tc.setupMongoMocks != nil {
				tc.setupMongoMocks(mt, mongo.keyRing)
			}
			// Add our mock response (either the success
			// or error as per test case definitions)
			mt.AddMockResponses(tc.mongoRes)

			// Call the method we are actually testing and ensure that
			// we got the correct error
			gotErr := mongo.StoreFollower(context.Background(), tc.followerID, tc.actorID)

			require.ErrorIs(mt, gotErr, tc.wantErr)
		})
	}
}

func TestGetFollowers(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name                  string
		getFollowerCollection func(mt *mtest.T, targetUserID string) *vocab.FollowersCollection
		prepMongoMock         func(mt *mtest.T, targetActorID string, fc *vocab.FollowersCollection)
		userName              string
		wantErr               error
	}{
		{
			name:     "three followers",
			userName: "alyssa",
			getFollowerCollection: func(mt *mtest.T, targetUserID string) *vocab.FollowersCollection {
				fc := vocab.NewFollowersCollection()
				fc.Items = append(fc.Items, vocab.Follower{ID: "follower1"})
				fc.Items = append(fc.Items, vocab.Follower{ID: "follower2"})
				fc.Items = append(fc.Items, vocab.Follower{ID: "follower3"})
				return fc
			},
			prepMongoMock: func(mt *mtest.T, targetActorID string, fc *vocab.FollowersCollection) {

				// Prep mock cursor responses

				for idx, follower := range fc.Items {
					// Marshal vocab to bson data
					f := Follower{
						FollowerID:    follower.ID,
						TargetActorID: targetActorID,
					}
					bsonData, err := bson.Marshal(f)
					require.NoError(mt, err)

					var bsonD bson.D
					// Unmarshal bson data to bson document
					err = bson.Unmarshal(bsonData, &bsonD)
					require.NoError(mt, err)

					batch := mtest.FirstBatch
					if idx > 0 {
						batch = mtest.NextBatch
					}

					res := mtest.CreateCursorResponse(
						1,
						fmt.Sprintf("%s.followers", testDbName),
						batch,
						bsonD)
					mt.AddMockResponses(res)
				}

				// Mock cursor end
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.followers", testDbName),
					mtest.NextBatch)

				// Add cursor mocks above to responses
				mt.AddMockResponses(end)
			},
		},
		{
			name:     "no followers",
			userName: "alyssa",
			getFollowerCollection: func(mt *mtest.T, targetUserID string) *vocab.FollowersCollection {
				fc := vocab.NewFollowersCollection()
				return fc
			},
			prepMongoMock: func(mt *mtest.T, targetActorID string, fc *vocab.FollowersCollection) {

				// Prep mock cursor responses

				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.followers", testDbName),
					mtest.FirstBatch)

				// Mock cursor end
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.followers", testDbName),
					mtest.NextBatch)

				// Add cursor mocks above to responses
				mt.AddMockResponses(res, end)
			},
		},
	}
	opts := mtest.NewOptions().DatabaseName(testDbName).ClientType(mtest.Mock)
	mt := mtest.New(t, opts)
	defer mt.Close()
	for _, tc := range testCases {
		tc := tc

		mt.Run(tc.name, func(mt *mtest.T) {
			mt.Parallel()

			// getTestMongo creates a Mongo instance with
			// a test database and a test keyring (to be
			// covered below)
			mongo := getTestMongo(mt)
			defer mongo.keyRing.DeleteKeys()

			// Get test case vocab and prepare mongo mocks
			actor := tc.getFollowerCollection(mt, tc.userName)
			tc.prepMongoMock(mt, tc.userName, actor)

			// Call the main function being tested
			gotFollowerCollection, gotErr := mongo.GetFollowers(context.Background(), tc.userName)
			require.ErrorIs(mt, gotErr, tc.wantErr)
			if gotErr != nil {
				return
			}

			// Verify returned fields
			require.NotNil(t, gotFollowerCollection)
		})
	}
}
