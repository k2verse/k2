package mongo

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/require"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
	"k2/server/keyring"
	"k2/server/vocab"
	"testing"
)

const (
	testDbName        = "k2-test"
	testClnActorsName = "actors"
	testDomain        = "https://test-domain.com"
	testMongoURI      = "mongodb://test"
)

// TestStoreActor tests storing an Actor instance in MongoDB
func TestStoreActor(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		// Test case name
		name string
		// Get vocab instance to store
		getActor func(mt *mtest.T) *vocab.Actor
		// The response we expect Mongo to return
		mongoRes bson.D
		// Desired error
		wantErr error
	}{
		{
			name: "success",
			getActor: func(mt *mtest.T) *vocab.Actor {
				// Just create a test vocab and return it
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, "alyssa")
				require.NoError(mt, err)
				return a
			},
			// Empty success response
			mongoRes: mtest.CreateSuccessResponse(),
		},
		{
			name: "failure",
			getActor: func(mt *mtest.T) *vocab.Actor {
				// Same vocab as above
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, "alyssa")
				require.NoError(mt, err)
				return a
			},
			// Have Mongo return a write error
			mongoRes: mtest.CreateWriteErrorsResponse(mtest.WriteError{
				Index:   1,
				Code:    123,
				Message: "some error",
			}),
			wantErr: ErrActorInsertFailed,
		},
	}

	opts := mtest.NewOptions().DatabaseName(testDbName).ClientType(mtest.Mock)

	// Create instance of `mtest.T`, which will contain a
	// test database.
	mt := mtest.New(t, opts)
	defer mt.Close()

	for _, tc := range testCases {
		// Shadow `tc` var to keep relevant test case in scope,
		// since these tests run in parallel
		tc := tc

		// mt.Run() creates a new T and a new collection for
		// the test case
		mt.Run(tc.name, func(mt *mtest.T) {
			mt.Parallel()
			// Add our mock response (either the success
			// or error as per test case definitions)
			mt.AddMockResponses(tc.mongoRes)

			// Create instance of our own Mongo struct, override the
			// database with the test one we created via `mtest.New()`
			mongo := NewMongo(testMongoURI, testDbName, nil)
			mongo.db = mt.DB

			actor := tc.getActor(mt)

			// Call the method we are actually testing and ensure that
			// we got the correct error
			gotErr := mongo.StoreActor(context.Background(), actor)
			require.ErrorIs(mt, gotErr, tc.wantErr)
		})
	}
}

func TestGetActor(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		name          string
		getActor      func(mt *mtest.T, userName string) *vocab.Actor
		prepMongoMock func(mt *mtest.T, ring keyring.KeyRing, actor *vocab.Actor)
		userName      string
		wantErr       error
	}{
		{
			name:     "success",
			userName: "alyssa",
			getActor: func(mt *mtest.T, userName string) *vocab.Actor {
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, userName)
				require.NoError(mt, err)
				return a
			},
			prepMongoMock: func(mt *mtest.T, ring keyring.KeyRing, actor *vocab.Actor) {
				bsonData, err := bson.Marshal(actor)
				require.NoError(mt, err)

				_, err = ring.GenerateKeyPair(actor.PreferredUsername)
				require.NoError(mt, err)

				var bsonD bson.D
				err = bson.Unmarshal(bsonData, &bsonD)
				require.NoError(mt, err)
				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.FirstBatch,
					bsonD)
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.NextBatch)
				mt.AddMockResponses(res, end)
			},
			wantErr: nil,
		},
		{
			name:     "not-found",
			userName: "otherUserName",
			getActor: func(mt *mtest.T, userName string) *vocab.Actor {
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, userName)
				require.NoError(mt, err)
				return a
			},
			prepMongoMock: func(mt *mtest.T, ring keyring.KeyRing, actor *vocab.Actor) {
				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.FirstBatch)
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.NextBatch)
				mt.AddMockResponses(res, end)
			},
			wantErr: ErrActorNotFound,
		},
	}

	opts := mtest.NewOptions().DatabaseName(testDbName).ClientType(mtest.Mock)
	mt := mtest.New(t, opts)
	defer mt.Close()
	for _, tc := range testCases {
		tc := tc

		mt.Run(tc.name, func(mt *mtest.T) {
			mt.Parallel()

			mongo := getTestMongo(mt)
			defer mongo.keyRing.DeleteKeys()

			actor := tc.getActor(mt, tc.userName)
			tc.prepMongoMock(mt, mongo.keyRing, actor)

			gotActor, gotErr := mongo.GetActor(context.Background(), actor.ID)
			require.ErrorIs(mt, gotErr, tc.wantErr)
			if gotErr != nil {
				return
			}

			require.NotEmpty(mt, gotActor.PublicKey)
			require.EqualValues(mt, actor.ID, gotActor.ID)
			require.EqualValues(mt, actor.PreferredUsername, gotActor.PreferredUsername)
			require.EqualValues(mt, actor.Type, gotActor.Type)
			require.EqualValues(mt, actor.Name, gotActor.Name)
			require.EqualValues(mt, actor.Icon, gotActor.Icon)
			require.EqualValues(mt, actor.Summary, gotActor.Summary)
		})
	}
}

func TestFindActorByPreferredName(t *testing.T) {
	t.Parallel()
	// Define test cases
	testCases := []struct {
		name          string
		getActor      func(mt *mtest.T, userName string) *vocab.Actor
		prepMongoMock func(mt *mtest.T, ring keyring.KeyRing, actor *vocab.Actor)
		userName      string
		wantErr       error
	}{
		{
			name:     "success",
			userName: "alyssa",
			getActor: func(mt *mtest.T, userName string) *vocab.Actor {
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, userName)
				require.NoError(mt, err)
				return a
			},
			prepMongoMock: func(mt *mtest.T, ring keyring.KeyRing, actor *vocab.Actor) {
				// Marshal vocab to bson data
				bsonData, err := bson.Marshal(actor)
				require.NoError(mt, err)

				// Generate test keypair
				_, err = ring.GenerateKeyPair(actor.PreferredUsername)
				require.NoError(mt, err)

				var bsonD bson.D
				// Unmarshal bson data to bson document
				err = bson.Unmarshal(bsonData, &bsonD)
				require.NoError(mt, err)

				// Prep mock cursor responses
				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.actors", testDbName),
					mtest.FirstBatch,
					bsonD)

				// Mock cursor end
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.actors", testDbName),
					mtest.NextBatch)

				// Add cursor mocks above to responses
				mt.AddMockResponses(res, end)
			},
		},
		{
			name:     "not-found",
			userName: "alyssa",
			getActor: func(mt *mtest.T, userName string) *vocab.Actor {
				a, err := vocab.NewActor(vocab.TypePerson, testDomain, userName)
				require.NoError(mt, err)
				return a
			},
			prepMongoMock: func(mt *mtest.T, ring keyring.KeyRing, actor *vocab.Actor) {
				// Mock response with no data
				res := mtest.CreateCursorResponse(
					1,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.FirstBatch)
				end := mtest.CreateCursorResponse(
					0,
					fmt.Sprintf("%s.%s", testDbName, testClnActorsName),
					mtest.NextBatch)
				mt.AddMockResponses(res, end)
			},
			wantErr: ErrActorNotFound,
		},
	}
	opts := mtest.NewOptions().DatabaseName(testDbName).ClientType(mtest.Mock)
	mt := mtest.New(t, opts)
	defer mt.Close()
	for _, tc := range testCases {
		tc := tc

		mt.Run(tc.name, func(mt *mtest.T) {
			mt.Parallel()

			// getTestMongo creates a Mongo instance with
			// a test database and a test keyring (to be
			// covered below)
			mongo := getTestMongo(mt)
			defer mongo.keyRing.DeleteKeys()

			// Get test case vocab and prepare mongo mocks
			actor := tc.getActor(mt, tc.userName)
			tc.prepMongoMock(mt, mongo.keyRing, actor)

			// Call the main function being tested
			gotActor, gotErr := mongo.FindActorByPreferredName(context.Background(), actor.PreferredUsername)
			require.ErrorIs(mt, gotErr, tc.wantErr)
			if gotErr != nil {
				return
			}

			// Verify returned fields
			require.NotEmpty(mt, gotActor.PublicKey)
			require.EqualValues(mt, actor.ID, gotActor.ID)
			require.EqualValues(mt, actor.PreferredUsername, gotActor.PreferredUsername)
			require.EqualValues(mt, actor.Type, gotActor.Type)
			require.EqualValues(mt, actor.Name, gotActor.Name)
			require.EqualValues(mt, actor.Icon, gotActor.Icon)
			require.EqualValues(mt, actor.Summary, gotActor.Summary)
		})
	}
}
