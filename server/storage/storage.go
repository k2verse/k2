package storage

import (
	"context"
	"k2/server/vocab"
)

//go:generate go run github.com/golang/mock/mockgen@v1.6.0 -package mocks -destination=./mocks/mock_storage.go -source=storage.go

type Storage interface {
	Connect(ctx context.Context) error
	StoreActor(ctx context.Context, actor *vocab.Actor) error
	GetActor(ctx context.Context, id string) (*vocab.Actor, error)
	FindActorByPreferredName(ctx context.Context, preferredName string) (*vocab.Actor, error)
	StoreFollower(ctx context.Context, followerID string, targetActorID string) error
	GetFollowers(ctx context.Context, targetActorID string) (*vocab.FollowersCollection, error)
}
