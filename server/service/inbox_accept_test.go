package service

import (
	"context"
	"fmt"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAcceptActivity(t *testing.T) {
	testCases := []struct {
		name               string
		actorID            string
		wantErr            error
		externalStatusCode int
		externalActorBody  string
	}{
		{
			name:               "success",
			actorID:            "from-sally",
			externalStatusCode: http.StatusOK,
			externalActorBody: `
			{
			  "@context": ["https://www.w3.org/ns/activitystreams",
						   {"@language": "ja"}],
			  "type": "Person",
			  "id": "https://kenzoishii.example.com/",
			  "following": "https://kenzoishii.example.com/following.json",
			  "followers": "https://kenzoishii.example.com/followers.json",
			  "liked": "https://kenzoishii.example.com/liked.json",
			  "inbox": "https://kenzoishii.example.com/inbox.json",
			  "outbox": "https://kenzoishii.example.com/feed.json",
			  "preferredUsername": "kenzoishii",
			  "name": "石井健蔵",
			  "summary": "この方はただの例です",
			  "icon": [
				"https://kenzoishii.example.com/image/165987aklre4"
			  ]
			}`,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			service := New(123, nil, "https://test.com")

			testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.WriteHeader(tc.externalStatusCode)
				_, err := w.Write([]byte(tc.externalActorBody))
				require.NoError(t, err)
			}))

			defer testServer.Close()

			objectID := fmt.Sprintf("%s/john", testServer.URL)
			gotErr := service.acceptFollow(context.TODO(), tc.actorID, objectID)
			require.ErrorIs(t, gotErr, tc.wantErr)
		})
	}
}
