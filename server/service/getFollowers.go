package service

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"k2/server/storage/mongo"
	"net/http"
)

func (s *Service) getFollowers(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userName, ok := vars["userName"]
	if !ok {
		http.Error(w, ErrNoUserName, http.StatusBadRequest)
		return
	}
	followers, err := s.storage.GetFollowers(r.Context(), userName)
	if err != nil || followers == nil {
		logrus.Error("failed to find vocab: %w", err)
		if errors.Is(err, mongo.ErrActorNotFound) {
			http.Error(w, "actor not found", http.StatusNotFound)
			return
		}
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(followers)
	if err != nil {
		logrus.Errorf("failed to marshal followers collection: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
