package service

import (
	"context"
	"github.com/sirupsen/logrus"
	actor2 "k2/server/vocab"
	"net/http"
)

func (s *Service) createActor(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	logrus.Infof("Parsed request form: %v", r.Form.Encode())
	userName := r.Form.Get("userName")
	if userName == "" {
		http.Error(w, ErrNoUserName, http.StatusBadRequest)
		return
	}
	actor, err := actor2.NewActor(actor2.TypePerson, s.domain, userName)
	if err != nil {
		logrus.Errorf("failed to create vocab: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	ctx := context.Background()
	if err := s.storage.StoreActor(ctx, actor); err != nil {
		logrus.Errorf("failed to store vocab: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/text")
	w.Write([]byte("Success"))
	/*vocab, err = s.storage.GetActor(ctx, vocab.ID)
	if err != nil {
		logrus.Error("failed to get vocab: %w", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	} */
}
