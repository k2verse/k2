package service

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"k2/server/storage/mongo"
	"net/http"
)

func (s *Service) getActor(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userName, ok := vars["userName"]
	if !ok {
		http.Error(w, ErrNoUserName, http.StatusBadRequest)
		return
	}
	actor, err := s.storage.FindActorByPreferredName(r.Context(), userName)
	if err != nil {
		if errors.Is(err, mongo.ErrActorNotFound) {
			http.Error(w, "actor not found", http.StatusNotFound)
			return
		}
		logrus.Error("failed to get vocab: %w", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(actor)
	if err != nil {
		logrus.Errorf("failed to marshal vocab: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(data)
}
