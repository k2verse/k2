package service

import (
	"errors"
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"k2/server/storage/mocks"
	"k2/server/storage/mongo"
	actor2 "k2/server/vocab"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
)

func TestCreateActor(t *testing.T) {
	testCases := []struct {
		name       string
		data       url.Values
		wantStatus int
		mockPrep   func(mockStorage *mocks.MockStorage)
	}{
		{
			name: "legit-username",
			data: url.Values{
				"userName": {"legitUserName"},
			},
			wantStatus: http.StatusCreated,
			mockPrep: func(mockStorage *mocks.MockStorage) {
				mockStorage.EXPECT().StoreActor(gomock.Any(), gomock.Any()).Return(nil)
			},
		}, {
			name:       "missing-username",
			data:       url.Values{},
			wantStatus: http.StatusBadRequest,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			service := New(123, nil, "https://test.com")
			mockCtrl := gomock.NewController(t)
			mockStorage := mocks.NewMockStorage(mockCtrl)
			if tc.mockPrep != nil {
				tc.mockPrep(mockStorage)
			}
			service.storage = mockStorage

			req, err := http.NewRequest("POST", "/actor", nil)
			require.NoError(t, err)
			req.Form = tc.data

			res := httptest.NewRecorder()
			service.createActor(res, req)

			require.EqualValues(t, tc.wantStatus, res.Code)
		})
	}
}

func TestGetActor(t *testing.T) {
	testCases := []struct {
		name       string
		userName   string
		wantStatus int
		wantBody   string
		mockPrep   func(mockStorage *mocks.MockStorage)
	}{
		{
			name:       "success",
			userName:   "alyssa",
			wantStatus: http.StatusOK,
			wantBody: `{"@context": ["https://www.w3.org/ns/activitystreams"],
					 "type": "Person",
					 "id": "https://social.example/alyssa",
					 "preferredUsername": "alyssa",
					 "inbox": "https://social.example/alyssa/inbox.json",
					 "outbox": "https://social.example/alyssa/outbox.json",
					 "followers": "https://social.example/alyssa/followers.json",
					 "following": "https://social.example/alyssa/following.json",
					 "liked": "https://social.example/alyssa/liked.json"}`,
			mockPrep: func(mockStorage *mocks.MockStorage) {
				actor, err := actor2.NewActor(actor2.TypePerson, "https://social.example", "alyssa")
				require.NoError(t, err)
				mockStorage.EXPECT().FindActorByPreferredName(gomock.Any(), gomock.Any()).Return(actor, nil)
			},
		},
		{
			name:       "actor-not-found",
			userName:   "alyssa",
			wantStatus: http.StatusNotFound,
			mockPrep: func(mockStorage *mocks.MockStorage) {
				mockStorage.EXPECT().FindActorByPreferredName(gomock.Any(), gomock.Any()).Return(nil, mongo.ErrActorNotFound)
			},
		},
		{
			name:       "internal-server-error",
			userName:   "alyssa",
			wantStatus: http.StatusInternalServerError,
			mockPrep: func(mockStorage *mocks.MockStorage) {
				mockStorage.EXPECT().FindActorByPreferredName(gomock.Any(), gomock.Any()).Return(nil, errors.New("some mongo error"))
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			service := New(123, nil, "https://social.example")
			mockCtrl := gomock.NewController(t)
			mockStorage := mocks.NewMockStorage(mockCtrl)
			if tc.mockPrep != nil {
				tc.mockPrep(mockStorage)
			}
			service.storage = mockStorage

			r, _ := http.NewRequest("GET", fmt.Sprintf("/%s", tc.userName), nil)
			w := httptest.NewRecorder()

			// Hack to try to fake gorilla/mux vars
			vars := map[string]string{
				"userName": tc.userName,
			}

			r = mux.SetURLVars(r, vars)

			service.getActor(w, r)
			require.EqualValues(t, tc.wantStatus, w.Code)
			if tc.wantBody != "" {
				require.JSONEq(t, tc.wantBody, w.Body.String())
			}
		})
	}
}
