package service

import (
	"errors"
	"fmt"
)

var (
	ErrNoUserName   = "request must contain username"
	ErrInvalidActor = errors.New("invalid actor")
)

func NewErrInvalidActor(userName string) error {
	return fmt.Errorf("%s: %w", userName, ErrInvalidActor)
}
