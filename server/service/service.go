package service

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"k2/server/storage"
	"k2/server/vocab"
	"net/http"
	"time"
)

type Service struct {
	srv     *http.Server
	storage storage.Storage
	domain  string
}

func New(port int, storage storage.Storage, domain string) *Service {
	router := &Service{
		storage: storage,
		domain:  domain,
	}
	r := mux.NewRouter()
	r.HandleFunc("/actor", router.createActor).Methods("POST")
	r.HandleFunc("/{userName}", router.getActor).Methods("GET")
	r.HandleFunc(fmt.Sprintf("/{userName}/%s", vocab.EndpointFollowers), router.getFollowers).Methods("GET")
	r.HandleFunc(fmt.Sprintf("/{userName}/%s", vocab.EndpointInbox), router.handleInbox).Methods("POST")

	srv := &http.Server{
		Handler:      r,
		Addr:         fmt.Sprintf(":%d", port),
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	router.srv = srv
	return router
}

func (s *Service) Serve() {
	logrus.Info("Connecting to storage")
	s.storage.Connect(context.Background())
	logrus.Infof("starting server at address %s", s.srv.Addr)
	logrus.Fatal(s.srv.ListenAndServe())
}
