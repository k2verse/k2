package service

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"k2/server/vocab"
	"net/http"
	"strings"
)

func (s *Service) handleInbox(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userName, ok := vars["userName"]
	if !ok {
		http.Error(w, ErrNoUserName, http.StatusBadRequest)
		return
	}
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		errMsg := "failed to parse body"
		logrus.Errorf("%s: %v", errMsg, err)
		http.Error(w, errMsg, http.StatusInternalServerError)
	}
	activity, err := vocab.ToActivity(body)
	targetActor, err := s.storage.FindActorByPreferredName(r.Context(), userName)
	if err != nil {
		e := NewErrInvalidActor(userName)
		http.Error(w, e.Error(), http.StatusNotFound)
		return
	}

	if err := s.storage.StoreFollower(r.Context(), activity.Actor.ID, targetActor.ID); err != nil {
		errMsg := "failed to store follower"
		logrus.Errorf("%s: %v", errMsg, err)
		http.Error(w, errMsg, http.StatusInternalServerError)
		return
	}
	// Send back Accept activity
	go func() {
		if err := s.acceptFollow(r.Context(), targetActor.ID, activity.Actor.ID); err != nil {
			logrus.Errorf("failed to accept follow: %v", err)
		}
	}()
}

func (s *Service) acceptFollow(ctx context.Context, actorID string, objectID string) error {
	activity := vocab.NewAcceptActivity(vocab.ActivityTypeFollow)
	activity.Actor = vocab.NewActorPersonWithID(actorID)
	activity.Object = vocab.Object{ID: objectID}

	// Get URL of actor's inbox
	res, err := http.Get(objectID)
	if err != nil {
		return fmt.Errorf("failed to get actor: %w", err)
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code when retrieving actor: %d", res.StatusCode)
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("failed to read body: %w", err)
	}
	var actor vocab.Actor
	if err := json.Unmarshal(body, &actor); err != nil {
		return fmt.Errorf("failed to umarshal json to external actor: %w", err)
	}
	inbox := actor.Inbox()
	accept := vocab.NewAcceptActivity(vocab.ActivityTypeFollow)
	data, err := json.Marshal(accept)
	if err != nil {
		return fmt.Errorf("failed to marshal accept activity: %w", err)
	}
	res, err = http.Post(inbox, "application/ld+json", strings.NewReader(string(data)))
	if err != nil {
		return fmt.Errorf("failed to post acceptance: %w", err)
	}
	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %d", res.StatusCode)
	}
	return nil
}
