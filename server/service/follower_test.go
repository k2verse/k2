package service

import (
	"fmt"
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"k2/server/storage/mocks"
	"k2/server/storage/mongo"
	actor2 "k2/server/vocab"
	"net/http"
	"net/http/httptest"
	"testing"
)

const testDomain = "https://test-domain.com"

func TestGetFollowers(t *testing.T) {
	testCases := []struct {
		name       string
		userName   string
		wantStatus int
		wantBody   string
		mockPrep   func(t *testing.T, mockStorage *mocks.MockStorage)
	}{
		{
			name:       "success",
			userName:   "alyssa",
			wantStatus: http.StatusOK,
			wantBody: `{"@context": "https://www.w3.org/ns/activitystreams",
					 "items": [
						{
							"id": "follower1"
                     	},
						{
							"id": "follower2"
                     	},
						{
							"id": "follower3"
                     	}
					 ],
					 "summary": "Followers of this actor",
					 "totalItems": 3,
					 "type": "Collection"}`,
			mockPrep: func(t *testing.T, mockStorage *mocks.MockStorage) {
				followers := actor2.NewFollowersCollection()
				followers.Items = []actor2.Follower{
					{
						ID: "follower1",
					},
					{
						ID: "follower2",
					},
					{
						ID: "follower3",
					},
				}
				mockStorage.EXPECT().GetFollowers(gomock.Any(), "alyssa").Return(followers, nil)
			},
		},
		{
			name:       "actor-not-found",
			userName:   "alyssa",
			wantStatus: http.StatusNotFound,
			mockPrep: func(t *testing.T, mockStorage *mocks.MockStorage) {
				mockStorage.EXPECT().GetFollowers(gomock.Any(), gomock.Any()).Return(nil, mongo.ErrActorNotFound)
			},
		},
		{
			name:       "no-followers-found",
			userName:   "alyssa",
			wantStatus: http.StatusOK,
			mockPrep: func(t *testing.T, mockStorage *mocks.MockStorage) {
				followers := actor2.NewFollowersCollection()
				mockStorage.EXPECT().GetFollowers(gomock.Any(), "alyssa").Return(followers, nil)
			},
		},
		{
			name:       "nil-response",
			userName:   "alyssa",
			wantStatus: http.StatusInternalServerError,
			mockPrep: func(t *testing.T, mockStorage *mocks.MockStorage) {
				mockStorage.EXPECT().GetFollowers(gomock.Any(), "alyssa").Return(nil, nil)
			},
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			service := New(123, nil, "https://social.example")
			mockCtrl := gomock.NewController(t)
			mockStorage := mocks.NewMockStorage(mockCtrl)
			if tc.mockPrep != nil {
				tc.mockPrep(t, mockStorage)
			}
			service.storage = mockStorage

			r, _ := http.NewRequest("GET", fmt.Sprintf("/%s", tc.userName), nil)
			w := httptest.NewRecorder()

			//Hack to try to fake gorilla/mux vars
			vars := map[string]string{
				"userName": tc.userName,
			}

			r = mux.SetURLVars(r, vars)

			service.getFollowers(w, r)
			require.EqualValues(t, tc.wantStatus, w.Code)
			if tc.wantBody != "" {
				require.JSONEq(t, tc.wantBody, w.Body.String())
			}
		})
	}
}
