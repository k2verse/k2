package service

import (
	"github.com/golang/mock/gomock"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/require"
	"k2/server/storage/mocks"
	"k2/server/vocab"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestFollowActivity(t *testing.T) {
	testCases := []struct {
		name       string
		muxVars    map[string]string
		bodyJson   string
		wantStatus int
		mockPrep   func(mockStorage *mocks.MockStorage)
	}{
		{
			name: "basic follow success",
			muxVars: map[string]string{
				"userName": "john",
			},
			bodyJson: `{
			  "@context": "https://www.w3.org/ns/activitystreams",
			  "summary": "Sally followed John",
			  "type": "Follow",
			  "actor": {
				"type": "Person",
				"id": "sally-id"
			  },
			  "object": "john-id"
			}`,
			mockPrep: func(mockStorage *mocks.MockStorage) {
				mockStorage.EXPECT().FindActorByPreferredName(gomock.Any(), "john").Return(vocab.NewActorPersonWithID("john-id"), nil)
				mockStorage.EXPECT().StoreFollower(gomock.Any(), "sally-id", "john-id").Return(nil)
			},
			wantStatus: http.StatusOK,
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			service := New(123, nil, "https://test.com")
			mockCtrl := gomock.NewController(t)
			mockStorage := mocks.NewMockStorage(mockCtrl)
			if tc.mockPrep != nil {
				tc.mockPrep(mockStorage)
			}
			service.storage = mockStorage

			req, err := http.NewRequest("POST", "/john/inbox.json", strings.NewReader(tc.bodyJson))
			require.NoError(t, err)

			req = mux.SetURLVars(req, tc.muxVars)
			res := httptest.NewRecorder()
			service.handleInbox(res, req)
			require.EqualValues(t, tc.wantStatus, res.Code)
		})
	}
}
