package keyring

import (
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"os"
	"path"
	"testing"
)

func TestLocalKeyring(t *testing.T) {
	testCases := []struct {
		name                 string
		keyID                string
		targetLocationExists bool
		locationFileMode     os.FileMode
		wantErr              error
	}{
		{
			name:                 "success",
			keyID:                "someUsername",
			targetLocationExists: true,
			locationFileMode:     0700,
		},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			td, err := ioutil.TempDir("", tc.name)
			require.NoError(t, err)

			locationDir := path.Join(td, "dir")
			defer os.RemoveAll(td)
			if tc.targetLocationExists {
				err = os.Mkdir(locationDir, tc.locationFileMode)
				require.NoError(t, err)
			}
			l := NewLocalRing(locationDir)
			pub, gotErr := l.GenerateKeyPair(tc.keyID)
			require.ErrorIs(t, gotErr, tc.wantErr)
			if gotErr != nil {
				return
			}
			require.NotNil(t, pub)
		})
	}
}
