package keyring

type KeyRing interface {
	GenerateKeyPair(fileName string) ([]byte, error)
	GetPublicKey(id string) ([]byte, error)
	DeleteKeys() error
}
