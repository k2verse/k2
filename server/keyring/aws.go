package keyring

import "errors"

var ErrNotImplemented = errors.New("this keyring is not yet implemented")

type AWSRing struct {
}

func (r *AWSRing) GenerateKeyPair(_ string) ([]byte, error) {
	return nil, ErrNotImplemented
}

func (r *AWSRing) GetPublicKey(_ string) ([]byte, error) {
	return nil, ErrNotImplemented
}

func (r *AWSRing) DeleteKeys() error {
	return ErrNotImplemented
}
