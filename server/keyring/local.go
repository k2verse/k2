package keyring

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"os"
	"path"
)

type LocalRing struct {
	location string
}

func NewLocalRing(location string) *LocalRing {
	return &LocalRing{location: location}
}

func (r *LocalRing) GetPublicKey(id string) ([]byte, error) {
	fileName := r.getPublicKeyFilename(id)
	contents, err := ioutil.ReadFile(fileName)
	if err != nil {
		return nil, fmt.Errorf("failed to read public key file: %w", err)
	}
	return contents, nil
}

func (r *LocalRing) GenerateKeyPair(id string) ([]byte, error) {
	if _, err := os.Stat(r.location); os.IsNotExist(err) {
		if err := os.Mkdir(r.location, 0700); err != nil {
			return nil, fmt.Errorf("failed to create key location: %w", err)
		}
	}

	key, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return nil, fmt.Errorf("failed to generate key: %w", err)
	}

	pub := key.Public()

	keyPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(key),
		},
	)

	pubPEM := pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: x509.MarshalPKCS1PublicKey(pub.(*rsa.PublicKey)),
		},
	)

	privKeyFilename := r.getPrivateKeyFilename(id)
	if err := ioutil.WriteFile(privKeyFilename, keyPEM, 0700); err != nil {
		return nil, fmt.Errorf("failed to write private key to file: %w", err)
	}

	pubKeyFilename := r.getPublicKeyFilename(id)
	if err := ioutil.WriteFile(pubKeyFilename, pubPEM, 0755); err != nil {
		return nil, fmt.Errorf("failed to write public key to file: %w", err)
	}
	return pubPEM, nil
}

func (r *LocalRing) DeleteKeys() error {
	err := os.RemoveAll(r.location)
	if err != nil {
		return fmt.Errorf("failed to delete keys: %w", err)
	}
	return nil
}

func (r *LocalRing) getPublicKeyFilename(id string) string {
	fileName := fmt.Sprintf("%s.rsa.pub", id)
	return path.Join(r.location, fileName)
}

func (r *LocalRing) getPrivateKeyFilename(id string) string {
	fileName := fmt.Sprintf("%s.rsa", id)
	return path.Join(r.location, fileName)
}
