package main

import (
	"github.com/jessevdk/go-flags"
	"github.com/sirupsen/logrus"
	"k2/server"
	"k2/server/keyring"
)

func main() {
	var opts struct {
		Store           string `long:"store" choice:"mongo" default:"mongo"`
		DbURI           string `long:"db-uri" description:"DB URI" default:"mongodb://localhost:27017"`
		DbName          string `long:"db-name" description:"DB Name" default:"k2"`
		UseLocalKeyRing bool   `long:"use-local-keyring" description:"Whether to use the local key ring instead of AWS"`
		Port            int    `long:"port" default:"8080"`
		Domain          string `long:"domain" default:"http://localhost"`
	}

	_, err := flags.Parse(&opts)
	if err != nil {
		logrus.Fatalf("failed to parse args: %v", err)
	}

	var keyRing keyring.KeyRing
	if opts.UseLocalKeyRing {
		logrus.Info("Using local keyring")
		keyRing = &keyring.LocalRing{}
	}
	s := server.NewServer(keyRing, opts.Domain, opts.Port, opts.DbURI, opts.DbName)
	s.Start()
}
